﻿using UnityEngine;

using System.Collections.Generic;

/// <summary>
/// Interpolation between points with a Catmull-Rom spline
/// </summary>
namespace SwipeTracking
{
    public static class Spline
    {
        #region Public Methods
        /// <summary>
        /// Compute Catmull-Rom spline interpolation for an array of path vertices
        /// </summary>
        /// <param name="vertices">Array of vertices</param>
        /// <param name="resolution">Resolution of interpolation</param>
        /// <returns>Array of vertices in spline</returns>
        public static Vector2[] CatmullRom(Vector2[] vertices, float resolution)
        {
            var spline = new List<Vector2>();

            for (int i = 0; i < vertices.Length - 1; i++)
                spline.AddRange(SplineSegment(vertices, i, resolution));

            return spline.ToArray();
        }

        /// <summary>
        /// Replicates each vertex in an array of vertices n times
        /// </summary>
        /// <param name="vertices">Array of vertices</param>
        /// <param name="n">Number of replications</param>
        /// <returns></returns>
        public static IEnumerable<Vector2> Replicate(Vector2[] vertices, int n)
        {
            foreach (var vertex in vertices)
            {
                for (var i = 0; i < n; i++) yield return vertex;
            }
        }
        #endregion Public Methods

        #region Private Methods
        /// <summary>
        /// Compute spline segment between 2 points derived by Catmull-Rom spline interpolation
        /// </summary>
        /// <param name="vertices">Array of vertices</param>
        /// <param name="index">Starting index of segment to interpolate</param>
        /// <param name="resolution">Resolution of interpolation</param>
        /// <returns>Yields interpolated vertices</returns>
        private static IEnumerable<Vector2> SplineSegment(Vector2[] vertices, int index, float resolution)
        {
            var p0 = vertices[Mathf.Clamp(index - 1, 0, vertices.Length - 1)];
            var p1 = vertices[index];
            var p2 = vertices[Mathf.Clamp(index + 1, 0, vertices.Length - 1)];
            var p3 = vertices[Mathf.Clamp(index + 2, 0, vertices.Length - 1)];

            yield return p1;

            for (var i = 1; i <= Mathf.FloorToInt(1f / resolution); i++)
            {
                var t = i * resolution;
                var a = 2f * p1;
                var b = p2 - p0;
                var c = 2f * p0 - 5f * p1 + 4f * p2 - p3;
                var d = -p0 + 3f * p1 - 3f * p2 + p3;

                yield return (a + (b * t) + (c * t * t) + (d * t * t * t)) / 2f;
            }
        }
        #endregion Private Methods
    }
}