﻿using UnityEngine;

#pragma warning disable CS0649

namespace SwipeTracking
{
    public class Test : MonoBehaviour
    {
        [SerializeField] private SwipeTrackerUI SwipeTrackerUI;

        private void Start()
        {
            SwipeTrackerUI.Initialise();
            SwipeTrackerUI.CreateTarget(0.5f);
            SwipeTrackerUI.CreateTarget(0f);
        }

        public void LogMessage(string message)
        {
            Debug.Log(message);
        }

        public void LogPosition(float position)
        {
            Debug.Log("New position: " + position);
        }
    }
}