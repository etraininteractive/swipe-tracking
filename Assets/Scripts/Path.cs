﻿using UnityEngine;

using System.Linq;
using System.Collections.Generic;

namespace SwipeTracking
{
    public class Path
    {
        #region Type Definitions
        public class Vertex
        {
            public Vector2 Position;
            public float Normalised;
        }

        private struct Segment
        {
            public Vertex StartVertex;
            public Vertex EndVertex;
        }
        #endregion Type Definitions

        #region Fields
        private Segment[] _path;
        #endregion Fields

        #region Public Methods
        /// <summary>
        /// Creates path from array of vertices
        /// </summary>
        /// <param name="vertices">Array of vertices</param>
        public Path(Vector2[] vertices)
        {
            _path = ToSegments(vertices).ToArray();

            Normalise(_path);
        }

        /// <summary>
        /// Project a point onto the path, i.e., determine the closest point on the path to point
        /// </summary>
        /// <param name="position">Point to project</param>
        /// <param name="reference">Reference point</param>
        /// <returns>Point on path</returns>
        public Vertex Project(Vector2 position, Vertex reference)
        {
            return _path
                .Select(segment => Project(segment, position))
                .Where(vertex => reference == null || Mathf.Abs(vertex.Normalised - reference.Normalised) < 0.1f)
                .Aggregate((x, y) => (x.Position - position).sqrMagnitude > (y.Position - position).sqrMagnitude ? y : x);
        }

        /// <summary>
        /// Get position on path by normalised value
        /// </summary>
        /// <param name="normalised">Normalised value</param>
        /// <returns>Position on path</returns>
        public Vertex NormalisedPosition(float normalised)
        {
            var segment = _path.First(s => s.StartVertex.Normalised <= normalised && s.EndVertex.Normalised >= normalised);

            return new Vertex
            {
                Position = Vector2.Lerp(segment.StartVertex.Position, segment.EndVertex.Position, (normalised - segment.StartVertex.Normalised) / (segment.EndVertex.Normalised - segment.StartVertex.Normalised)),
                Normalised = normalised
            };
        }
        #endregion Public Methods

        #region Private Methods
        /// <summary>
        /// Project a point onto a path segment, i.e., determine the closest point on the segment to a point
        /// </summary>
        /// <param name="segment">Segment to project onto</param>
        /// <param name="position">Point to project</param>
        /// <returns>Point on segment</returns>
        private static Vertex Project(Segment segment, Vector2 position)
        {
            var normal = segment.EndVertex.Position - segment.StartVertex.Position;
            var t = Mathf.Clamp(Vector2.Dot(position - segment.StartVertex.Position, normal) / normal.sqrMagnitude, 0f, 1f);

            return new Vertex
            {
                Position = Vector2.Lerp(segment.StartVertex.Position, segment.EndVertex.Position, t),
                Normalised = Mathf.Lerp(segment.StartVertex.Normalised, segment.EndVertex.Normalised, t)
            };
        }

        /// <summary>
        /// Convert an array of positions to an array of segments
        /// </summary>
        /// <param name="position">Array of positions</param>
        /// <returns>Array of segments</returns>
        private static IEnumerable<Segment> ToSegments(Vector2[] position)
        {
            var length = 0f;

            for (var i = 1; i < position.Length; i++) yield return new Segment
            {
                StartVertex = new Vertex
                {
                    Position = position[i - 1],
                    Normalised = length
                },
                EndVertex = new Vertex
                {
                    Position = position[i],
                    Normalised = (length += (position[i - 1] - position[i]).magnitude)
                }
            };
        }

        /// <summary>
        /// Compute normalised lengths for an array of segments
        /// </summary>
        /// <param name="segments">Array of segments to normalise</param>
        private static void Normalise(Segment[] segments)
        {
            var factor = 1f / segments.Last().EndVertex.Normalised;

            for (var i = 0; i < segments.Length; i++)
            {
                segments[i].StartVertex.Normalised *= factor;
                segments[i].EndVertex.Normalised *= factor;
            }
        }
        #endregion Private Methods
    }
}