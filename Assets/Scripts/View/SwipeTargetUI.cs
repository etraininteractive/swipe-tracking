﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

/// <summary>
/// SwipeTargetUI requires a Graphic (e.g., Image) attached to the gameObject with Raycast Target = true for the
/// EventSystem handlers to work
/// </summary>

namespace SwipeTracking
{
    [RequireComponent(typeof(Graphic))]
    public class SwipeTargetUI : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler, IDragHandler
    {
        #region Type Definitions
        public class SwipeEvent : UnityEvent<SwipeTargetUI> { }
        #endregion Type Definitions

        #region Public Properties
        public bool active { get; set; }

        public Path.Vertex origin
        {
            get { return _origin; }
            set { _origin = position = value; }
        }

        public Path.Vertex position
        {
            get { return _position; }
            set
            {
                _position = value;
                transform.localPosition = value.Position;
            }
        }

        public SwipeEvent onDrag { get; private set; }
        public SwipeEvent onExit { get; private set; }
        #endregion Public Properties

        #region Private Variables
        private Path.Vertex _origin;
        private Path.Vertex _position;
        #endregion Private Variables

        #region Class Constructor
        SwipeTargetUI()
        {
            onDrag = new SwipeEvent();
            onExit = new SwipeEvent();
        }
        #endregion Class Constructor

        #region Public Methods
        public void ResetToOrigin()
        {
            position = origin;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            active = true;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (active) onExit.Invoke(this);

            active = false;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (active) onExit.Invoke(this);

            active = false;
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (active) onDrag.Invoke(this);
        }
        #endregion Public Methods
    }
}