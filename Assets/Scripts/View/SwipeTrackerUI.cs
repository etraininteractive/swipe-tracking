﻿using UnityEngine;
using UnityEngine.Events;

using System;
using System.Linq;

#pragma warning disable CS0649

namespace SwipeTracking
{
    public class SwipeTrackerUI : MonoBehaviour
    {
        #region Type Definitions
        [Serializable] public class ValueChangedEvent : UnityEvent<float> { }
        #endregion Type Definitions

        #region Variables
        [SerializeField] private Camera _camera;
        [SerializeField] private RectTransform _canvas;
        [SerializeField] private SwipeTargetUI _targetPrefab;
        [SerializeField] private bool _allowBacktrack;
        [SerializeField] private bool _drawPath;
        [SerializeField] private bool _smooth;
        [SerializeField] private LineRenderer _lineRenderer;
        [SerializeField] private Transform[] _targets;
        [SerializeField] private ValueChangedEvent _onValueChanged;
        [SerializeField] private UnityEvent _onSuccess;
        [SerializeField] private UnityEvent _onInterrupt;

        private Path _path;
        #endregion Variables

        #region Public Methods
        /// <summary>
        /// Creates and draws path
        /// </summary>
        public void Initialise()
        {
            var vertices = _targets.Select(target => WorldToCanvas(target.transform.position)).ToArray();
            var path = _smooth ? Spline.CatmullRom(vertices, 0.1f) : vertices;

            _path = new Path(path);

            if (_drawPath)
            {
                var verticesWorld = (_smooth ? path : Spline.Replicate(path, 3)).Select(vertex => transform.TransformPoint(vertex)).ToArray();

                _lineRenderer.positionCount = verticesWorld.Length;
                _lineRenderer.SetPositions(verticesWorld);
            }
        }

        /// <summary>
        /// Creates a UI target at a normalised position
        /// </summary>
        /// <param name="normalisedPosition">Normalised position</param>
        /// <returns>UI target</returns>
        public SwipeTargetUI CreateTarget(float normalisedPosition)
        {
            var target = Instantiate(_targetPrefab, transform);

            target.origin = _path.NormalisedPosition(normalisedPosition);
            target.onDrag.AddListener(OnDrag);
            target.onExit.AddListener(t => _onInterrupt.Invoke());
            target.onExit.AddListener(t => t.ResetToOrigin());

            return target;
        }
        #endregion Public Methods

        #region Private Methods
        /// <summary>
        /// Converts a world point to a point on the canvas
        /// </summary>
        /// <param name="position">World position</param>
        /// <returns>Canvas position</returns>
        private Vector2 WorldToCanvas(Vector3 position)
        {
            var viewport = _camera.WorldToViewportPoint(position);

            return new Vector2((viewport.x - 0.5f) * _canvas.sizeDelta.x, (viewport.y - 0.5f) * _canvas.sizeDelta.y);
        }

        /// <summary>
        /// Event handler for target drag action
        /// </summary>
        /// <param name="target">Dragged target</param>
        private void OnDrag(SwipeTargetUI target)
        {
            var canvasSize = _canvas.GetComponent<RectTransform>().sizeDelta;
            var mousePosition  = new Vector2(
                (Input.mousePosition.x / Screen.width - 0.5f)  * canvasSize.x, 
                (Input.mousePosition.y / Screen.height - 0.5f) * canvasSize.y);

            var projection = _path.Project(mousePosition, target.position);

            if (!_allowBacktrack && projection.Normalised < target.position.Normalised) return;

            target.position = projection;

            _onValueChanged.Invoke(projection.Normalised);

            if (projection.Normalised > 1f - 1e-3)
            {
                target.active = false;
                target.ResetToOrigin();
                _onSuccess.Invoke();
            }
        }
        #endregion Private Methods
    }
}